#!/bin/bash

# Utiliser -backend-config lors de l'initialisation pour configurer le backend
sudo date -s "$(curl -s --head http://google.com | grep ^Date: | sed 's/Date: //g')"
terraform init -backend-config=backend.hcl -migrate-state
terraform init -backend-config=backend.hcl -reconfigure
terraform plan -var-file=variables.tfvars
terraform apply -var-file=variables.tfvars -auto-approve
