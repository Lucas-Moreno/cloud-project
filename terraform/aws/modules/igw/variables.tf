variable "igw_name" {
  description = "Name of the internet gateway"
  type        = string
}

variable "vpc_id" {
  type = string
}
