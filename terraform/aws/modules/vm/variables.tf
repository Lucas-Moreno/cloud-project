variable "instance_type" {
  description = "EC2 instance type"
  type        = string
}

variable "instance_ami" {
  description = "AMI ID for the EC2 instance"
  type        = string
}

variable "ssh_key_name" {
  description = "Name of the SSH key pair"
  type        = string
}

variable "instance_tags" {
  type = map(string)
}

variable "subnet_id" {
  type = string
}

variable "security_group_id" {
  type = string
}

variable "root_block_device" {
  description = "Configuration of the root block device for the EC2 instance."
  type        = object({
    volume_type = string
    volume_size = number
  })
  default = {
    volume_type = "gp2"
    volume_size = 20
  }
}