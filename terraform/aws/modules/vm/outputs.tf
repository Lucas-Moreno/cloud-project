output "public_ip" {
  description = "The public IP of the instance"
  value       = aws_instance.tclo_instance.public_ip
}