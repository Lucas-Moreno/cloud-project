resource "aws_instance" "tclo_instance" {
  ami           = var.instance_ami
  instance_type = var.instance_type
  key_name      = var.ssh_key_name
  subnet_id     = var.subnet_id
  tags          = var.instance_tags

  vpc_security_group_ids = [var.security_group_id]

  root_block_device {
    volume_type = var.root_block_device.volume_type
    volume_size = var.root_block_device.volume_size
  }

}
