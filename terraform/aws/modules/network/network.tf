resource "aws_vpc" "tclo_vpc" {
  cidr_block = var.vpc_cidr_block
  tags       = var.vpc_tags
}

resource "aws_subnet" "tclo_subnet" {
  vpc_id                  = aws_vpc.tclo_vpc.id
  cidr_block              = var.subnet_cidr_block
  availability_zone       = var.availability_zone
  map_public_ip_on_launch = true
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.tclo_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.igw_id
  }

}

resource "aws_route_table_association" "public_subnet" {
  subnet_id      = aws_subnet.tclo_subnet.id
  route_table_id = aws_route_table.public.id
}
