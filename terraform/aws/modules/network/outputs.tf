output "subnet_id" {
  value = aws_subnet.tclo_subnet.id
}

output "vpc_id" {
  value = aws_vpc.tclo_vpc.id
}