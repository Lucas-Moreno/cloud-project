terraform {
  backend "http" {}

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.31.0"
    }
  }
}

provider "aws" {
  region      = "eu-west-3"
  access_key  = var.aws_access_key_id
  secret_key  = var.aws_secret_access_key
}

resource "aws_route53_zone" "main" {
  name = "lucas-moreno.net"
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "lucas-moreno.net"
  type    = "A"
  ttl     = "300" 
  records = [module.vm.public_ip]
}

resource "aws_route53_record" "subdomains" {
  for_each = toset(["front", "mongo", "back", "traefik"])
  
  zone_id = aws_route53_zone.main.zone_id
  name    = "${each.value}.lucas-moreno.net"
  type    = "A"
  ttl     = "300"
  records = [module.vm.public_ip]
}

module "network" {
  source = "./modules/network"

  vpc_cidr_block     = "10.0.0.0/16"
  vpc_tags           = { 
    Name = "TCLO-VPC" 
  }
  subnet_cidr_block  = "10.0.1.0/24"
  availability_zone  = "eu-west-3a"
  igw_id             = module.igw.igw_id
}

module "vm" {
  source = "./modules/vm"

  instance_ami       = "ami-0f014d1f920a73971"
  instance_type      = "t2.medium"
  ssh_key_name       = "key-tclo-vm"
  instance_tags      = {
    Name = "TCLO-VM"
  }

  root_block_device = {
    volume_type = "gp2"
    volume_size = 20
  }
  
  subnet_id          = module.network.subnet_id
  security_group_id  = module.security_group.security_group_id
}

module "security_group" {
  source = "./modules/security_group"

  instance_type          = "t2.medium"
  security_group_name    = "TCLO-SECURITY-GROUP"
  security_group_ingress = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 8081
      to_port     = 8081
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 3001
      to_port     = 3001
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 3000
      to_port     = 3000
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 27017
      to_port     = 27017
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = -1
      to_port     = -1
      protocol    = "icmp"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]
  security_group_egress  = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]
  vpc_id = module.network.vpc_id
}

module "igw" {
  source = "./modules/igw"

  igw_name = "TCLO-IGW"
  vpc_id   = module.network.vpc_id
}