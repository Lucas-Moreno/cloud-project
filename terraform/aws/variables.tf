variable "aws_access_key_id" {
  description = "Access key"
  type        = string
}

variable "aws_secret_access_key" {
  description = "Secret key"
  type        = string
}

variable "instance_tags" {
  description = "Tags for the EC2 instance"
  type        = map(string)
  default     = {
    Name = "TCLO-VM"
  }
}

variable "security_group_egress" {
  description = "Egress rules for the security group"
  type        = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
  default = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]
}
