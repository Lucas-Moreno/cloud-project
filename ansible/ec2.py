#!/usr/bin/python3

# import os
import sys
import boto3
import json
# import subprocess
from env import vars

# ntpdate_command = "sudo ntpdate us.pool.ntp.org"
# try:
#     subprocess.run(ntpdate_command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
# except subprocess.CalledProcessError as e:
#     print(f"Erreur lors de l'exécution de la commande ntpdate : {e.stderr.decode('utf-8').strip()}")


def read_settings():
    aws_access_key = vars.AWS_ACCESS_KEY_ID
    aws_secret_key = vars.AWS_SECRET_ACCESS_KEY
    aws_region = vars.AWS_REGION

    # GITLAB
    key_pem="/root/.ssh/key-tclo-vm.pem"
    # LOCAL
    # key_pem=vars.PATH_KEY_PEM
    
    if aws_access_key is None or aws_secret_key is None:
        print("AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY environment variables are not defined in the .env file of the ansible directory.")
        sys.exit(1)

    return aws_access_key, aws_secret_key, aws_region, key_pem

def connect_to_aws(aws_access_key, aws_secret_key, aws_region):
    try:
        session = boto3.Session(
            aws_access_key_id=aws_access_key,
            aws_secret_access_key=aws_secret_key,
            region_name=aws_region
        )
        ec2 = session.client('ec2')
        return ec2
    except Exception as e:
        print(f"Erreur de connexion à la région AWS: {e}")
        return None
 
def generate_inventory(ec2):
    inventory = {'all': {'hosts': []}, '_meta': {'hostvars': {}}}

    if ec2:
        try:
            response = ec2.describe_instances()
            instances = [i for r in response['Reservations'] for i in r['Instances']]

            for instance in instances:
                ip_address = instance.get('PublicIpAddress')
                if ip_address:
                    inventory['all']['hosts'].append(ip_address)
                    inventory['_meta']['hostvars'][ip_address] = {
                        'instance_id': instance['InstanceId'],
                        'ansible_ssh_private_key_file': key_pem,
                        'instance_user': 'admin'
                    }

            print(json.dumps(inventory, indent=2))

        except Exception as e:
            print(f"Erreur lors de la récupération des instances: {e}")
            sys.exit(1)
    else:
        print("La connexion à la région AWS a échoué.")
        sys.exit(1)

    
aws_access_key, aws_secret_key, aws_region, key_pem = read_settings()
ec2 = connect_to_aws(aws_access_key, aws_secret_key, aws_region)
generate_inventory(ec2)
