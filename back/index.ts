import bodyParser from "body-parser";
import cors from "cors";
import dotenv from "dotenv";
import express from "express";
import "./src/models/db";
import router from "./src/routes/routes";
import { Express } from "./src/types/types";

dotenv.config();

const PORT = 3001;

const app: Express = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const corsOptions = {
  origin: [
    "http://localhost:3000",
    "http://front.lucas-moreno.net",
    "http://back.lucas-moreno.net",
  ],
  credentials: true,
  optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));

app.use("/", router);

app.listen(PORT, () => {
  console.log(`Server is listening at http://localhost:${PORT}`);
});

module.exports = app;
