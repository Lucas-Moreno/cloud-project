import dotenv from "dotenv";
import mongoose, { ConnectOptions } from "mongoose";
import UserModel from "./user.models";

dotenv.config();

const username = "lucasmoreno";
const password = "lucasmoreno";
const host = "13.38.92.14";
const port = "27017";

const uri = `mongodb://${username}:${password}@${host}:${port}`;

if (!uri) {
  throw new Error("MONGO_URI environment variable is not defined");
}

mongoose.set("strictQuery", false);

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
} as ConnectOptions);

mongoose.connection.on("connected", () => {
  console.log("MongoDB Connection Succeeded");
});

mongoose.connection.on("error", (err) => {
  console.log("Error in db connection: " + err);
});

export { UserModel };
