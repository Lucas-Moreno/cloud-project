import express, { Request, Response } from "express";
import mongoose, { ConnectOptions } from "mongoose";
import { logout, signin, signup } from "../controllers/auth.controller";
import { getInfoUser } from "../controllers/user.controller";
import { authMiddleware } from "../middleware/auth.middleware";
import { Router } from "../types/types";

const router: Router = express.Router();

// Welcome to my api
router.get("/", (req: Request, res: Response) => {
  res.send("Welcome to my API");
});

// Health
router.get("/health", (req: Request, res: Response) => {
  const uri = "mongodb://lucasmoreno:lucasmoreno@mongodb:27017/";

  mongoose.set("strictQuery", false);

  mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  } as ConnectOptions);

  mongoose.connection.on("connected", () => {
    res.send("MongoDB Connection Succeeded");
  });

  mongoose.connection.on("error", (err) => {
    res.send("Error in db connection: " + err);
  });
});

// Authentification
router.post("/signup", signup);
router.post("/signin", signin);
router.post("/logout", logout);

// User
router.get("/user", (req: Request, res: Response) => {
  authMiddleware(req, res, () => {
    getInfoUser(req, res);
  });
});

export default router;
