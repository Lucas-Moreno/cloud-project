Récupérer la liste des VPC créer :

aws ec2 describe-vpcs --region eu-west-3

Supprimer un VPC :

aws ec2 delete-vpc --vpc-id VPC_ID --region eu-west-3
